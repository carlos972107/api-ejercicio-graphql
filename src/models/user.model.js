const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

//construcion del schema para el mapeo del modelo User
const UserSchema = new Schema({
  email: { type: String, unique: true, lowercase: true },
  username: String,
  password: String,
  resetCode: String,
  resetExpiryTime: Date,
  signupDate: { type: Date, default: Date.now() },
  lastLogin: Date
});

//creacion de la contaseña segura antes de ingresarlo a la base de datos
UserSchema.pre("save", function(next) {
  let user = this;
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(user.password, salt);
  user.password = hash;
  next();
});

//comparacion de contraseña segura
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, res) {
    cb(null, res);
  });
};

module.exports = mongoose.model("User", UserSchema);