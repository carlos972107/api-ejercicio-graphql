const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//construcion del schema para el mapeo del modelo cliente
const ClienteSchema = new Schema({
    id: String,
    name: { type: String, uppercase: true },
    lastname: { type: String, uppercase: true },
    email: { type: String, lowercase: true },
    phone: String,
    locale: String,
});

  

module.exports = mongoose.model("Cliente", ClienteSchema);