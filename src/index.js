const { ApolloServer } = require("apollo-server");
const mongoose = require("mongoose");
const config = require("./config/config");
const resolvers = require("./lib/resolver");
const service = require("./service/index");
//llamado de depencias necesarias para el arranque

mongoose.Promise = require("bluebird");
//transformamos el entorno de conexion a mongodb en promesa

mongoose.set("useCreateIndex", true);
mongoose.connect(
  config.db, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  promiseLibrary: require("bluebird")
})
.then(() => console.log('DB Connected!'))
.catch(err => console.log(`DB Connection Error: ${err.message}`));
  //conexion a mongoDB

  //schema graphql para realizar las peticiones
const typeDefs = `
    type Query {
        clientes: [Cliente!]
        cliente(id: String!): Cliente
    }
    type Cliente {
        id: String!
        name: String
        lastname: String
        email: String
        phone: String
        locale: String
    }
    type Mutation {
        signup (username: String!, email: String!, password: String!): String
        login (email: String!, password: String!): String
        createCliente (name: String!, lastname: String!, email: String!, phone: String, locale: String): Cliente
        updateCliente (id: String!, name: String!, lastname: String!, email: String!, phone: String, locale: String): Cliente
        deleteCliente (id: String!): Cliente
    }
`;

//servidor con configuraciones de apollo sever
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    let authToken = null;
    let user = null;
    try {
      authToken = req.headers.authorization;
      if (authToken) {
        user = await service.decodeToken(authToken);
      }
    } catch (e) {
      console.warn(`No se pudo autenticar el token: ${authToken}`);
    }
    return {
      authToken,
      user
    };
  }
});

server.listen().then(({ url }) => {
  console.log(`🚀 Servidor corriendo en: ${url}`);
});