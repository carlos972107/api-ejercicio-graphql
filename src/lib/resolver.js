const User = require("../models/user.model");
const uniqid = require("uniqid");
const Cliente = require("../models/cliente.model");
const service = require("../service/index");
const bcrypt = require("bcryptjs");

const resolvers = {
  //query permitidas
  Query: {
    //query que permite la devolucion de todos los cliente existentes
    clientes: (parent, args, context) => {
      return new Promise((resolve, reject) => {
        Cliente.find((err, res) => {
          if(err) reject(err)
          return resolve(res)
        })
      })
    },
    //query que permite la devolucion del cliente con el id que coincida
    cliente: (parent, { id }) => {
      return new Promise((resolve, reject) => {
        Cliente.findOne({id}, (err, res) => {
          if(err) reject(err)
          return resolve(res)
        });
      });
    }
  },
  //mutaciones permitidas 
  Mutation: {
    //actualizacion de un cliente expecifico a traves de promesa
    updateCliente(_, { id, name, lastname, email, phone, locale }){
      return new Promise((resolve, reject) => {
        Cliente.findOneAndUpdate({id}, {"$set": {name, lastname, email, phone, locale}}).exec((err, res) => {
          if(err) reject(err);
          return resolve(res);
        })
      })
    },
    //borrar un cliente expecifico a traves de su id
    deleteCliente(_, { id }){
      return new Promise((resolve, reject) => {
        Cliente.findOneAndRemove({id}, (err, res) => {
          if(err) reject(err)
          return resolve(res)
        })
      })
    },
    //creacion de un cliente en la base de datos
    createCliente(_, { name, lastname, email, phone, locale }){
      let cliente = new Cliente();
      cliente.id = uniqid();
      cliente.name = name;
      cliente.lastname = lastname;
      cliente.email = email;
      cliente.phone = phone;
      cliente.locale = locale;
      return new Promise((resolve, reject) => {
        cliente.save((err, res) => {
          if(err) reject(err)
          return resolve(res)
        })
      });
    },
    // creacion de usuario
    signup(_, { username, email, password }) {
      let user = new User();
      user.username = username;
      user.email = email;
      user.password = password;
      return new Promise((resolve, reject) => {
        User.findOne({ email: email }, (err, existingUser) => {
          if (existingUser) {
            throw new Error(`Ya existe un usuario con esa cuenta de correo`);
          } else {
            user.save((err, user) => {
              if (err) reject(err);
              return resolve(service.createToken(user));
            });
          }
        });
      });
    },

    // inicio de sesion de un usuario
    async login(_, { email, password }) {
      const user = new Promise((resolve, reject) => {
        User.findOne({ email: email }, (err, res) => {
          if(err) reject(err)
          return resolve(res)
        });
      })
      if (!user) {
        throw new Error("no existe un usuario con ese email");
      }
      const valid = await bcrypt.compareSync(password, user.password);
      if (!valid) {
        throw new Error("contraseña incorrecta");
      }
      return service.createToken(user);
    }
  }
};

module.exports = resolvers;